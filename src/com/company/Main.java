package com.company;

import java.util.Arrays;

class Main {

    public static void main(String[] args) {
        CsvGenerator csvGenerator = new CsvGeneratorWriter();
        csvGenerator.toCsv(Arrays.asList("first line", "second line", "third line"));
    }
}