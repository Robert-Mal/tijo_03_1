package com.company;

import java.util.List;

interface CsvGenerator {
    void toCsv(List<String> lines);
}
