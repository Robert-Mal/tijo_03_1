package com.company;

import java.util.List;

public interface PdfGenerator {
    void toPdf(List<String> lines);
}
