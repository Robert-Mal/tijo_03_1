package com.company;

import java.io.FileWriter;
import java.io.IOException;
import java.util.List;

class CsvGeneratorWriter implements CsvGenerator {
    @Override
    public void toCsv(List<String> lines)
    {
        try {
            FileWriter CsvWriter = new FileWriter("new.csv");
            // pretty line
            prettyLine(CsvWriter);
            for (String line: lines) {
                CsvWriter.append(line);
            }
            CsvWriter.append("\n");
            // pretty line
            prettyLine(CsvWriter);
            CsvWriter.flush();
            CsvWriter.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    private void prettyLine(FileWriter fileWriter) {
        try {
            fileWriter.append("*************");
            fileWriter.append("\n");
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
